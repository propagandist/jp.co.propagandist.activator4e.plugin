package jp.co.propagandist.activator4e;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.function.Consumer;

public class InputStreamThread extends Thread {

	private BufferedReader br;

	private Consumer<String> handler;

	/** コンストラクター */
	public InputStreamThread(final InputStream is, final Consumer<String> consumer) {
		this.br = new BufferedReader(new InputStreamReader(Objects.requireNonNull(is)));
		this.handler = Objects.requireNonNull(consumer);
	}

	@Override
	public void run() {
		try {
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				handler.accept(line);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
