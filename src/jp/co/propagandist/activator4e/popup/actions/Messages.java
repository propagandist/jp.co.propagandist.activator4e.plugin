package jp.co.propagandist.activator4e.popup.actions;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "jp.co.propagandist.activator4e.popup.actions.messages"; //$NON-NLS-1$
	public static String BaseActivatorAction_SystemErrorMsg;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
