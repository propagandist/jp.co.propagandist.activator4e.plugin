package jp.co.propagandist.activator4e;

import java.util.Objects;

import org.eclipse.core.resources.IProject;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class MessageConsoleStreamWrapper implements AutoCloseable {

	private final MessageConsoleStream messageConsoleStream;
	private final IProject project;
	private final String prefix;

	public MessageConsoleStreamWrapper(final MessageConsoleStream messageConsoleStream, final IProject project) {
		this.messageConsoleStream = Objects.requireNonNull(messageConsoleStream);
		this.project = Objects.requireNonNull(project);
		this.prefix = String.format("%s: ", project.getName()); //$NON-NLS-1$
	}

	public void print(final String message) {
		this.messageConsoleStream.print(prefix);
		this.messageConsoleStream.print(message);
	}

	public void println() {
		this.messageConsoleStream.println();
	}

	public void println(final String message) {
		this.messageConsoleStream.print(prefix);
		this.messageConsoleStream.println(message);
	}

	public MessageConsole getConsole() {
		return this.messageConsoleStream.getConsole();
	}

	@Override
	public void close() throws Exception {
		this.messageConsoleStream.close();
	}

	public IProject getProject() {
		return this.project;
	}

	public String getPrefix() {
		return prefix;
	}

}
