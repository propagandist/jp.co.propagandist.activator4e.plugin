package jp.co.propagandist.activator4e.popup.actions;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import jp.co.propagandist.activator4e.Activator;
import jp.co.propagandist.activator4e.Commands.Command;
import jp.co.propagandist.activator4e.InputStreamThread;
import jp.co.propagandist.activator4e.MessageConsoleStreamWrapper;

public abstract class BaseActivatorAction extends BaseAction {

	@Override
	final public void run(IAction action) {
		final StructuredSelection ss = (StructuredSelection) selection;
		if (ss.isEmpty()) {
			return;
		}
		final IFile buildFile = (IFile) ss.getFirstElement();
		final IProject project = buildFile.getProject();
		if (project != null && project.exists() && project.isOpen()) {
			try {
				final Command command = getCommand();
				switch (command.getLaunchType()) {
				case InBackground:
					runInBackground(action, project, command);
					break;
				case InTerminal:
					runInTerminal(action, project, command);
					break;
				}
			} catch (CoreException ex) {
				Activator.log(ex.getStatus());
			}
		}
	}

	protected abstract Command getCommand();

	private void runInTerminal(final IAction action, final IProject project, final Command command)
			throws CoreException {
		DebugPlugin.exec(command.getCommandLine(), project.getLocation().toFile());
	}

	private void runInBackground(final IAction action, final IProject project, final Command... commands)
			throws CoreException {
		final File workingDirectory = project.getLocation().toFile();
		final Display display = workbenchPart.getSite().getShell().getDisplay();
		final MessageConsole console = Activator.getConsole();
		final Job job = new Job(Activator.PLUGIN_TITLE) {

			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				try (final MessageConsoleStreamWrapper stdOut = new MessageConsoleStreamWrapper(
						console.newMessageStream(), project);
						final MessageConsoleStream errOut = console.newMessageStream()) {
					display.syncExec(() -> {
						errOut.setColor(display.getSystemColor(SWT.COLOR_RED));
					});
					for (final Command command : commands) {
						monitor.beginTask(command.getTaskName(), -1);

						// プロセス開始
						final Process process = DebugPlugin.exec(command.getCommandLine(), workingDirectory);

						stdOut.println("=====> " + command.getTaskName()); //$NON-NLS-1$

						// スレッド開始
						final InputStreamThread stdThread = new InputStreamThread(process.getInputStream(),
								stdOut::println);
						stdThread.start();
						final InputStreamThread errThread = new InputStreamThread(process.getErrorStream(),
								errOut::println);
						errThread.start();

						// プロセス終了待ち
						process.waitFor();

						// スレッド終了待ち
						stdThread.join();
						errThread.join();
					}
					stdOut.println();

					return Status.OK_STATUS;
				} catch (CoreException ex) {
					return ex.getStatus();
				} catch (InterruptedException e) {
					return Status.CANCEL_STATUS;
				} catch (Throwable th) {
					return new Status(IStatus.ERROR, Activator.PLUGIN_ID, jp.co.propagandist.activator4e.popup.actions.Messages.BaseActivatorAction_SystemErrorMsg, th);
				} finally {
					monitor.done();
				}
			}

		};
		job.schedule();
	}

}
