package jp.co.propagandist.activator4e.popup.actions;

import jp.co.propagandist.activator4e.Commands;
import jp.co.propagandist.activator4e.Commands.Command;

public class RunTaskAction extends BaseActivatorAction {

	@Override
	protected Command getCommand() {
		return Commands.TASK_RUN;
	}

}
