package jp.co.propagandist.activator4e;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "jp.co.propagandist.activator4e.plugin"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	/** プラグイン件名 */
	public static final String PLUGIN_TITLE = Messages.Activator_PluginName;

	/** コンソール名 */
	public static final String CONSOLE_NAME = Messages.Activator_ConsoleName;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
	 * BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.
	 * BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static MessageConsole getConsole() {
		return getDefault().findConsole(CONSOLE_NAME);
	}

	public MessageConsole findConsole(final String name) {
		final ConsolePlugin plugin = ConsolePlugin.getDefault();
		final IConsoleManager conMan = plugin.getConsoleManager();
		final IConsole[] existing = conMan.getConsoles();

		MessageConsole console = null;
		for (int i = 0; i < existing.length; i++) {
			if (name.equals(existing[i].getName())) {
				console = (MessageConsole) existing[i];
			}
		}
		if (console == null) {
			console = new MessageConsole(name, null);
			conMan.addConsoles(new IConsole[] { console });
		}
		conMan.showConsoleView(console);

		return console;
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 *
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	public static void log(final IStatus status) {
		getDefault().getLog().log(status);
	}

	public static IStatus log(int severity, final String message) {
		final IStatus status = new Status(severity, PLUGIN_ID, message);
		getDefault().getLog().log(status);
		return status;
	}

	public static IStatus log(int severity, final String message, final Throwable throwable) {
		final IStatus status = new Status(severity, PLUGIN_ID, message, throwable);
		getDefault().getLog().log(status);
		return status;
	}

	public static Shell getShell() {
		return getDefault().getWorkbench().getActiveWorkbenchWindow().getShell();
	}

}
