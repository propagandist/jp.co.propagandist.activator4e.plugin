package jp.co.propagandist.activator4e;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "jp.co.propagandist.activator4e.messages"; //$NON-NLS-1$
	public static String Activator_ConsoleName;
	public static String Activator_PluginName;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
