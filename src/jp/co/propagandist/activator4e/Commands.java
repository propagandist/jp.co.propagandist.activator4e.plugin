package jp.co.propagandist.activator4e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Commands {

	private static final String[] IN_TERMINAL = new String[] { "cmd.exe", "/c", "start" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	private static final String[] IN_BACKGROUND = new String[] { "cmd.exe", "/c" }; //$NON-NLS-1$ //$NON-NLS-2$

	/*
	 * In Terminal
	 */
	public static Command ACTIVATOR = new Command(LaunchType.InTerminal, "activator"); //$NON-NLS-1$
	public static Command ACTIVATOR_IN_DEBUG = new Command(LaunchType.InTerminal, "activator", "-jvm-debug"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_RUN = new Command(LaunchType.InTerminal, "activator", "run"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_RUN_IN_DEBUG = new Command(LaunchType.InTerminal, "activator", "-jvm-debug", //$NON-NLS-1$ //$NON-NLS-2$
			"run"); //$NON-NLS-1$

	/*
	 * In Background
	 */
	public static Command CMD_LAST = new Command(LaunchType.InBackground, "activator", "last"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command CMD_RELOAD = new Command(LaunchType.InBackground, "activator", "reload"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_CLEAN = new Command(LaunchType.InBackground, "activator", "clean"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_COMPILE = new Command(LaunchType.InBackground, "activator", "compile"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_COPY_RESOURCES = new Command(LaunchType.InBackground, "activator", //$NON-NLS-1$
			"copyResources"); //$NON-NLS-1$
	public static Command TASK_DOC = new Command(LaunchType.InBackground, "activator", "doc"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_GEN_WITHOUT_SRC = new Command(LaunchType.InBackground, "activator", //$NON-NLS-1$
			"\"eclipse with-source=false\""); //$NON-NLS-1$
	public static Command TASK_GEN_WITH_SRC = new Command(LaunchType.InBackground, "activator", //$NON-NLS-1$
			"\"eclipse with-source=true\""); //$NON-NLS-1$
	public static Command TASK_PACKAGE = new Command(LaunchType.InBackground, "activator", "package"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_PACKAGE_BIN = new Command(LaunchType.InBackground, "activator", "packageBin"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_PACKAGE_DOC = new Command(LaunchType.InBackground, "activator", "packageDoc"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_PACKAGE_SRC = new Command(LaunchType.InBackground, "activator", "packageSrc"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_PUBLISH = new Command(LaunchType.InBackground, "activator", "publish"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_PUBLISH_LOCAL = new Command(LaunchType.InBackground, "activator", //$NON-NLS-1$
			"publishLocal"); //$NON-NLS-1$
	public static Command TASK_PUBLISH_M2 = new Command(LaunchType.InBackground, "activator", "publishM2"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_TEST = new Command(LaunchType.InBackground, "activator", "test"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_TEST_ONLY = new Command(LaunchType.InBackground, "activator", "testOnly"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_TEST_QUICK = new Command(LaunchType.InBackground, "activator", "testQuick"); //$NON-NLS-1$ //$NON-NLS-2$
	public static Command TASK_UPDATE = new Command(LaunchType.InBackground, "activator", "update"); //$NON-NLS-1$ //$NON-NLS-2$

	public static enum LaunchType {
		InTerminal, InBackground;
	}

	public static class Command {
		private LaunchType launchType;
		private String[] commandLine;
		private String taskName;

		public Command(LaunchType launchType, String... additions) {
			this.launchType = Objects.requireNonNull(launchType);
			String[] base = null;
			switch (launchType) {
			case InBackground:
				base = IN_BACKGROUND;
				break;
			case InTerminal:
				base = IN_TERMINAL;
				break;
			}
			this.commandLine = merge(base, additions);
			this.taskName = Arrays.stream(additions).collect(Collectors.joining(" ")); //$NON-NLS-1$
		}

		private String[] merge(final String[] base, final String... additions) {
			List<String> args = new ArrayList<String>(Arrays.asList(base));
			for (String s : additions) {
				args.add(s);
			}
			return args.toArray(new String[] {});
		}

		public LaunchType getLaunchType() {
			return launchType;
		}

		public String[] getCommandLine() {
			return this.commandLine;
		}

		public String getTaskName() {
			return this.taskName;
		}

	}
}
