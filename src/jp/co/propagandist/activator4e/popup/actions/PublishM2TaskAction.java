package jp.co.propagandist.activator4e.popup.actions;

import jp.co.propagandist.activator4e.Commands;
import jp.co.propagandist.activator4e.Commands.Command;

public class PublishM2TaskAction extends BaseActivatorAction {

	@Override
	protected Command getCommand() {
		return Commands.TASK_PUBLISH_M2;
	}

}
